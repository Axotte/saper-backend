import express = require("express");
import * as http from "http";
import * as WebSocket from "ws";
import {AddressInfo, MessageEvent} from "ws";
import {EventType} from "./enums/EventTypesEnum";
import {Event} from "./models/EventModel";
import {Game} from "./models/GameModel";
import {IncomingEvent} from "./models/IncomingEventModel";

const app = express();

const server = http.createServer(app);

const wss = new WebSocket.Server({ server });

let games: Game[] = [];
let queue: WebSocket = null;

wss.on("connection", (ws: WebSocket) => {
    handleNewConnection(ws);
    ws.onclose = () => {
        handleDisconnection(ws);
    };
    ws.onmessage = (message: MessageEvent) => {
        try {
            const json: any = JSON.parse(message.data.toString());
            if (json.type === EventType.PLAY_AGAIN) {
                handleNewConnection(ws);
            } else {
                const event: IncomingEvent = new IncomingEvent(json.type, json.message, ws);
                getUsersGame(ws).handleIncomingEvent(event);
            }
        } catch (e) {
            console.log("Cannot find users game");
        }
    };
});

server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port ${(server.address() as AddressInfo).port}`);
});

const handleNewConnection = (newConnection: WebSocket) => {
    if (queue === null) {
        newConnection.send(JSON.stringify(new Event(EventType.WAITING_FOR_PLAYER, "")));
        queue = newConnection;
    } else {
        console.log("New game created");
        const game: Game = new Game(queue, newConnection);
        games.push(game);
        queue = null;
        game.sendGameCreatedEvent();
    }
};

const handleDisconnection = (disconnectedUser: WebSocket) => {
    if (disconnectedUser === queue) {
        queue = null;
    } else {
        games = games.filter((game: Game) => {
           const usersGame: boolean = game.player1 === disconnectedUser || game.player2 === disconnectedUser;
           if (usersGame) {
               game.sendUserDisconnectedEvent(disconnectedUser);
           }
           return !usersGame;
        });
    }
};

const getUsersGame = (user: WebSocket): Game => {
    return games.filter((game: Game) => (user === game.player1 || user === game.player2) && !game.gameFinished)[0];
};
