import {EventType} from "../enums/EventTypesEnum";

export class Event {
    public type: EventType;
    public message: any;

    constructor(type: EventType, message: any) {
        this.type = type;
        this.message = message;
    }
}
