import {Observable, Subscription, timer} from "rxjs";
import * as WebSocket from "ws";
import {EventType} from "../enums/EventTypesEnum";
import {Event} from "./EventModel";
import {Field} from "./Field";
import {IncomingEvent} from "./IncomingEventModel";

export class Game {

    private static sendEvent(to: WebSocket, type: EventType, message: any) {
        to.send(JSON.stringify(new Event(type, message)));
    }
    public player1: WebSocket;
    public player2: WebSocket;
    public gameFinished: boolean = false;
    private field: Field;
    private nextMove: WebSocket;
    private player1Name: string = null;
    private player2Name: string = null;
    private isGameStarted: boolean = false;
    private player1WantRevenge: boolean = null;
    private player2WantRevenge: boolean = null;
    private timeToDefeat: number = 10;
    private ticker: Observable<number> = timer(0, 100);
    private subscription: Subscription = this.ticker.subscribe((_) => {
        if (!this.isGameStarted) { return; }
        this.timeToDefeat -= 0.1;
        this.sendBroadcastEvent(EventType.TIME_CHANGED, this.timeToDefeat);
        if (this.timeToDefeat <= 0) {
            Game.sendEvent(this.nextMove, EventType.YOU_LOSE, this.field.hiddenField);
            this.swapNextMove();
            Game.sendEvent(this.nextMove, EventType.YOU_WIN, this.field.hiddenField);
            this.gameFinished = true;
            this.subscription.unsubscribe();
        }
    });

    constructor(player1: WebSocket, player2: WebSocket) {
        this.player1 = player1;
        this.player2 = player2;
        this.nextMove = player1;
        this.field = new Field();
    }

    public sendGameCreatedEvent() {
        Game.sendEvent(this.player1, EventType.GAME_CREATED, "");
        Game.sendEvent(this.player2, EventType.GAME_CREATED, "");
    }

    public sendUserDisconnectedEvent(disconnectedUser: WebSocket) {
        if (this.isGameStarted || this.player1Name === null || this.player2Name === null) {
            if (disconnectedUser === this.player1) {
                Game.sendEvent(this.player2, EventType.OPPONENT_DISCONNECTED, "");
            } else if (disconnectedUser === this.player2) {
                Game.sendEvent(this.player1, EventType.OPPONENT_DISCONNECTED, "");
            }
            this.isGameStarted = false;
        }
    }

    public handleIncomingEvent(event: IncomingEvent) {
        if (event.type === EventType.MAKE_MOVE) {
            this.handleMakeMoveEvent(event);
        } else if (event.type === EventType.INTRODUCE_YOURSELF) {
            this.handleIntroduceEvent(event);
        } else if (event.type === EventType.WANT_REVENGE) {
            this.handleRevengeEvent(event);
        }
    }

    private handleIntroduceEvent(event: IncomingEvent) {
        if (event.from === this.player1) {
            this.player1Name = event.message;
        } else {
            this.player2Name = event.message;
        }
        if (this.player1Name !== null && this.player2Name !== null) {
            Game.sendEvent(this.player1, EventType.GAME_STARTED,
                {opponentsName: this.player2Name, fieldSize: Field.FIELD_SIZE, yourName: this.player1Name});
            Game.sendEvent(this.player2, EventType.GAME_STARTED,
                    {opponentsName: this.player1Name, fieldSize: Field.FIELD_SIZE, yourName: this.player2Name});
            Game.sendEvent(this.nextMove, EventType.YOUR_MOVE, this.field.visibleField);
            Game.sendEvent(this.getOpponent(this.nextMove), EventType.OPPONENT_MOVE, this.field.visibleField);
            this.isGameStarted = true;
        }
    }

    private getOpponent(player: WebSocket): WebSocket {
        return player === this.player1 ? this.player2 : this.player1;
    }

    private handleRevengeEvent(event: IncomingEvent) {
        if (event.from === this.player1) {
            this.player1WantRevenge = true;
        } else {
            this.player2WantRevenge = true;
        }
        if (this.player1WantRevenge && this.player2WantRevenge) {
            this.field = new Field();
            this.player2WantRevenge = false;
            this.player1WantRevenge = false;
            Game.sendEvent(this.player1, EventType.GAME_STARTED, this.player2Name);
            Game.sendEvent(this.player2, EventType.GAME_STARTED, this.player1Name);
            this.isGameStarted = true;
        }
    }

    private handleMakeMoveEvent(event: IncomingEvent) {
        if (!this.isGameStarted) {
            Game.sendEvent(event.from, EventType.GAME_NOT_STARTED, "Game not started yet");
            return;
        }
        if (event.from !== this.nextMove) {
            Game.sendEvent(event.from, EventType.ILLEGAL_MOVE, "Not your turn");
        } else {
            try {
                if (this.field.makeMove(event.message)) {
                    if (this.field.isGameFinished()) {
                        this.sendBroadcastEvent(EventType.DRAW, this.field.hiddenField);
                        this.isGameStarted = false;
                        this.subscription.unsubscribe();
                    } else {
                        Game.sendEvent(this.nextMove, EventType.OPPONENT_MOVE,  this.field.visibleField);
                        this.swapNextMove();
                        Game.sendEvent(this.nextMove, EventType.YOUR_MOVE,  this.field.visibleField);
                        this.timeToDefeat = 10;
                    }
                } else {
                    Game.sendEvent(this.nextMove, EventType.YOU_LOSE, this.field.hiddenField);
                    this.swapNextMove();
                    Game.sendEvent(this.nextMove, EventType.YOU_WIN, this.field.hiddenField);
                    this.gameFinished = true;
                    this.subscription.unsubscribe();
                }
            } catch (e) {
                Game.sendEvent(event.from, EventType.ILLEGAL_MOVE, "Illegal move");
            }
        }
    }

    private swapNextMove() {
        if (this.nextMove === this.player1) {
            this.nextMove = this.player2;
        } else {
            this.nextMove = this.player1;
        }
    }

    private sendBroadcastEvent(type, message) {
        this.player1.send(JSON.stringify(new Event(type, message)));
        this.player2.send(JSON.stringify(new Event(type, message)));
    }
}
