import * as WebSocket from "ws";
import {EventType} from "../enums/EventTypesEnum";

export class IncomingEvent {
    public type: EventType;
    public message: any;
    public from: WebSocket;

    constructor(type: EventType, message: any, webSocket: WebSocket) {
        this.type = type;
        this.message = message;
        this.from = webSocket;
    }
}
