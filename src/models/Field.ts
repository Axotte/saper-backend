export class Field {
    public static MINES = 10;
    public static FIELD_SIZE = 8;
    public visibleField: string[];
    public hiddenField: string[];
    private isFirstMove: boolean = true;
    private fieldsToWin: number = Field.FIELD_SIZE * Field.FIELD_SIZE - Field.MINES;

    constructor() {
        this.createNewField();
    }

    public createNewField() {
        this.visibleField = [];
        this.hiddenField = [];
        for (let i = 0; i < Field.FIELD_SIZE * Field.FIELD_SIZE; i++) {
            this.visibleField.push(" ");
            this.hiddenField.push(" ");
        }
        this.setupMines();
        this.countMiens();
    }

    public isGameFinished(): boolean {
        return this.fieldsToWin === 0;
    }

    public makeMove(fieldIndex: number): boolean {
        this.checkFirstMove(fieldIndex);
        if (this.visibleField[fieldIndex] !== " ") {
            throw Error("Illegal move");
        }
        if (this.hiddenField[fieldIndex] === "M") {
            return false;
        } else {
            this.showField(fieldIndex);
            return true;
        }
    }

    private showField(fieldIndex: number) {
        if (this.hiddenField[fieldIndex] === "M") { return; }
        this.visibleField[fieldIndex] = this.hiddenField[fieldIndex];
        this.fieldsToWin--;
        const x = Math.floor(fieldIndex / Field.FIELD_SIZE);
        const y = fieldIndex % Field.FIELD_SIZE;
        if (this.hiddenField[fieldIndex] === "0") {
            for (let i = -1; i <= 1; i++) {
                for (let j = -1; j <= 1; j++) {
                    const newX = x + i;
                    const newY = y + j;
                    const newIndex = newX * Field.FIELD_SIZE + newY;
                    if (newX < 0 || newX >= Field.FIELD_SIZE || newY < 0 || newY >= Field.FIELD_SIZE) { continue; }
                    if (this.visibleField[newIndex] === " " && this.hiddenField[newIndex] !== "M") {
                        this.showField(newIndex);
                    }
                }
            }
        }
    }

    private checkFirstMove(fieldIndex: number) {
        if (this.isFirstMove && this.hiddenField[fieldIndex] === "M") {
            while (this.hiddenField[fieldIndex] === "M") {
                this.createNewField();
            }
        }
        this.isFirstMove = false;
    }

    private setupMines() {
        let minesPlaced = 0;
        while (minesPlaced !== Field.MINES) {
            const random = Math.floor(Math.random() * (Field.FIELD_SIZE * Field.FIELD_SIZE));
            if (this.hiddenField[random] !== "M") {
                this.hiddenField[random] = "M";
                minesPlaced++;
            }
        }
    }

    private countMiens() {
        for (let i = 0; i < this.hiddenField.length; i++) {
            if (this.hiddenField[i] === "M") { continue; }
            const x = Math.floor(i / Field.FIELD_SIZE);
            const y = i % Field.FIELD_SIZE;
            let numberOfMines = 0;
            for (let j = -1; j <= 1; j++) {
                for (let k = -1; k <= 1; k++) {
                    if (x + j < 0 || x + j >= Field.FIELD_SIZE || y + k < 0 || y + k >= Field.FIELD_SIZE) { continue; }
                    if (x + j < 0 || x + j >= Field.FIELD_SIZE || y + k < 0 || y + k >= Field.FIELD_SIZE) { continue; }
                    const field = (x + j) * Field.FIELD_SIZE + (y + k);
                    if (this.hiddenField[field] === "M") { numberOfMines++; }
                }
            }
            this.hiddenField[i] = numberOfMines.toString();
        }
    }
}
